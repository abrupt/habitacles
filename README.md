# ~/ABRÜPT/JÉRÔME ORSONI/HABITACLES/*

La [page de ce livre](https://abrupt.cc/jerome-orsoni/habitacles/) sur le réseau.

## Sur le livre

Écriture par anticipations. Écriture pour habitacles. Pour habiter. Un espace masqué qui se découvre en avançant. Et soi en habitacle de ce qui s'y présente. La découverte simple. Point de mire. Simple en désirs. Simple voulant dire clair, la clarté étant l’instrument de la transformation — de soi, du monde, du langage. Y remettre de l'être, et y habiter en parallèle. Et rester fidèle — habitacles sans arrêt. Avec quelque chose en plus. Plus de sens. Plus de vie. De l'air entre les pensées. De l'espace. Comme par la grâce d’une idée fixe.

## Sur l'auteur

Jérôme Orsoni. Quelque part au bord de la Méditerranée. Y vit, y traduit et y écrit. Le reste n'ayant que peu d'importance.

[Ses cahiers fantômes](https://cahiersfantomes.com/). *Habitacles* est son septième livre.

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
