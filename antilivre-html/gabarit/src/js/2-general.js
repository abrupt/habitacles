// Scripts

// Musique
var audio = document.getElementById("fond-sonore");
// var musiqueJoue = "pause";
function fondSonore() {
    if (audio.paused) {
        audio.play();
    }
    else {
        audio.pause();
    }
};

// Menu
const menu = document.querySelector('.menu');
const titre = document.querySelector('.pagetitre');

menu.addEventListener('click', (e) => {
  e.preventDefault();
  titre.classList.toggle('show--menu');
  document.querySelector('.menu .icone--info-on').classList.toggle('none');
  document.querySelector('.menu .icone--info-off').classList.toggle('none');
});

const musique = document.querySelector('.musique');
musique.addEventListener('click', (e) => {
  e.preventDefault();
  document.querySelector('.musique .icone--info-on').classList.toggle('none');
  document.querySelector('.musique .icone--info-off').classList.toggle('none');
  fondSonore();
  // document.querySelector('.musique .icone--info-on').classList.toggle('none');
  // document.querySelector('.musique .icone--info-off').classList.toggle('none');
});

const close = document.querySelectorAll('.close');
close.forEach((btn) => {
  btn.addEventListener('click', (e) => {
    e.preventDefault();
    btn.parentNode.classList.remove('show--texte');
    btn.parentNode.classList.add('hide');
  });
});

// Panolens
let size = 2000;
let habitacles1, habitacles2, habitacles3, habitacles4, habitacles5, habitacles6, habitacles7;

// Panorama
habitacles1 = new PANOLENS.Infospot( size, cabane1 );
habitacles1.position.set( 1172.69, -325.13, -4841.86 );
habitacles1.addEventListener('click', () => {
  const texte = document.getElementById('habitacles-1');
  texte.classList.remove('hide');
  texte.classList.add('show--texte');
});
habitacles2 = new PANOLENS.Infospot( size, cabane2 );
habitacles2.position.set( 4370.65, -326.50, -2392.24 );
habitacles2.addEventListener('click', () => {
  const texte = document.getElementById('habitacles-2');
  texte.classList.remove('hide');
  texte.classList.add('show--texte');
});
habitacles3 = new PANOLENS.Infospot( size, cabane3 );
habitacles3.position.set( 4839.78, -565.05, 1091.38 );
habitacles3.addEventListener('click', () => {
  const texte = document.getElementById('habitacles-3');
  texte.classList.remove('hide');
  texte.classList.add('show--texte');
});
habitacles4 = new PANOLENS.Infospot( size, cabane4 );
habitacles4.position.set( 3225.39, -535.73, 3771.49 );
habitacles4.addEventListener('click', () => {
  const texte = document.getElementById('habitacles-4');
  texte.classList.remove('hide');
  texte.classList.add('show--texte');
});
habitacles5 = new PANOLENS.Infospot( size, cabane5 );
habitacles5.position.set( -623.31, -351.43, 4942.70 );
habitacles5.addEventListener('click', () => {
  const texte = document.getElementById('habitacles-5');
  texte.classList.remove('hide');
  texte.classList.add('show--texte');
});
habitacles6 = new PANOLENS.Infospot( size, cabane6 );
habitacles6.position.set( -4433.34, -636.25, 2200.01 );
habitacles6.addEventListener('click', () => {
  const texte = document.getElementById('habitacles-6');
  texte.classList.remove('hide');
  texte.classList.add('show--texte');
});
habitacles7 = new PANOLENS.Infospot( size, cabane7 );
habitacles7.position.set( -3707.86, -439.83, -3321.98 );
habitacles7.addEventListener('click', () => {
  const texte = document.getElementById('habitacles-7');
  texte.classList.remove('hide');
  texte.classList.add('show--texte');
});

let panorama, viewer;

if (navigator.onLine) {
  panorama = new PANOLENS.VideoPanorama( 'https://txt.abrupt.cc/antilivre/abrupt_orsoni_jerome_habitacles_antilivre.mp4', { autoplay: true, loop: true, muted: true } );
  // panorama = new PANOLENS.ImagePanorama( atmosphere );
} else {
  panorama = new PANOLENS.ImagePanorama( atmosphere );
}

panorama.add( habitacles1 );
panorama.add( habitacles2 );
panorama.add( habitacles3 );
panorama.add( habitacles4 );
panorama.add( habitacles5 );
panorama.add( habitacles6 );
panorama.add( habitacles7 );

viewer = new PANOLENS.Viewer({
  autoHideInfospot: false,
  controlButtons: ['fullscreen','setting']
});
viewer.add( panorama );
viewer.setCameraFov(70);

// Hide progress bar for video
// document.querySelector('.panolens-container div span:nth-child(4) span:nth-child(2)').style.display = 'none'

 // PANOLENS.Viewer.prototype.getPosition = function () {
 //   var intersects, point, panoramaWorldPosition, outputPosition;
 //   intersects = this.raycaster.intersectObject( this.panorama, true );

 //   if ( intersects.length > 0 ) {
 //     point = intersects[0].point;
 //     panoramaWorldPosition = this.panorama.getWorldPosition();

 //     // Panorama is scaled -1 on X axis
 //     outputPosition = new THREE.Vector3(
 //       -(point.x - panoramaWorldPosition.x).toFixed(2),
 //       (point.y - panoramaWorldPosition.y).toFixed(2),
 //       (point.z - panoramaWorldPosition.z).toFixed(2)
 //     );
 //   }

 //   return outputPosition;
 // };

 // panorama.addEventListener( 'click', function(){
 //     var clickPosition = viewer.getPosition(); //returns x,y,z
 //     console.log(" " + clickPosition.x + ", " + clickPosition.y + ", " + clickPosition.z + " ");
 // });

// document.querySelector('#musique').onclick = function () {
//   // if (musiqueJoue == "pause") {
//   //   musiqueJoue = "continue";
//   // } else {
//   //   musiqueJoue = "pause";
//   // };
//   document.querySelector('#musique .icone--music-on').classList.toggle('none');
//   document.querySelector('#musique .icone--music-off').classList.toggle('none');
//   // fondSonore();
// }

let touchEvent = 'ontouchstart' in window ? 'touchstart' : 'click';
let permissionValue = false;
let messageIOS = false;
// document.querySelector('.panolens-container div span:nth-child(1) a').addEventListener("click touchstart", function() {
document.querySelector('.panolens-container div span:nth-child(1) a').addEventListener(touchEvent, function() {
  // document.querySelector('.panolens-container div:nth-child(3) span:nth-child(5) a:nth-child(3)').addEventListener(touchEvent, function() {
// window.onload = function () {
  // alert("hello one");
  // Check if is IOS 13 when page loads.
  // Source : https://github.com/aframevr/aframe/issues/4287#issuecomment-548208060

    const platform = window.navigator.platform;
    const iosPlatforms = ['iPhone', 'iPad', 'iPod'];

  if ( !permissionValue && window.DeviceMotionEvent && typeof window.DeviceMotionEvent.requestPermission === 'function' ){
      // Everything here is just a lazy banner. You can do the banner your way.
      const banner = document.createElement('div')
    banner.innerHTML = `<div class="devicemotion" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#080264; color: #eef;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Cliquer sur cette bannière<br>pour activer le «&nbsp;Device Motion&nbsp;» (gyroscope) sur iOS<br>(effacer les données de navigation<br>pour changer de décision).</p></div>`
      banner.onclick = ClickRequestDeviceMotionEvent // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
    document.querySelector('body').appendChild(banner)
  } else if (!messageIOS && iosPlatforms.indexOf(platform) !== -1) {
      const banner = document.createElement('div')
    banner.innerHTML = `<div class="devicemotion--ios" style="z-index: 900; position: absolute; bottom: 0; left: 0; width: 100%; background-color:#080264; color: #eef;"><p style="font-style: 1em; padding: 2em 1em; text-align: center;">Pour les versions inférieures à iOS 13<br>activer le «&nbsp;Device Motion&nbsp;» (gyroscope)<br>dans Réglages > Safari > Confidentialité et Sécurité > Mouvement et Orientation<br>(cliquer sur cette bannière<br>pour la faire disparaître).</p></div>`
    banner.onclick = () => banner.remove(); // You NEED to bind the function into a onClick event. An artificial 'onClick' will NOT work.
    document.querySelector('body').appendChild(banner)
    messageIOS = true;
  }
  // });
});

function ClickRequestDeviceMotionEvent () {
  window.DeviceMotionEvent.requestPermission()
    .then(response => {
      if (response === 'granted') {
        window.addEventListener('devicemotion',
          () => { console.log('DeviceMotion permissions granted.') },
          (e) => { throw e }
      )
      } else {
        console.log('DeviceMotion permissions not granted.')
      }
      permissionValue = true;
      document.querySelector(".devicemotion").remove();
    })
    .catch(e => {
      console.error(e)
    })
}

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var content = document.querySelector('.pagetitre');
content.addEventListener('touchstart', function (event) {
    this.allowUp = (this.scrollTop > 0);
    this.allowDown = (this.scrollTop < this.scrollHeight - this.clientHeight);
    this.slideBeginY = event.pageY;
});

content.addEventListener('touchmove', function (event) {
    var up = (event.pageY > this.slideBeginY);
    var down = (event.pageY < this.slideBeginY);
    this.slideBeginY = event.pageY;
    if ((up && this.allowUp) || (down && this.allowDown)) {
        event.stopPropagation();
    }
    else {
        event.preventDefault();
    }
});

function checkPlay() {
  if (panorama.isVideoPaused()) {
    viewer.updateVideoPlayButton(true);
  } else {
    viewer.updateVideoPlayButton(false);
  }
}
