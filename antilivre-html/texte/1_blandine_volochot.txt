---
author: "Jérôme Orsoni"
title: "Habitacles"
publisher: "Abrüpt"
date: "août 2020"
lieu: 'Internet & Zürich'
description: ""
subject: "littérature"
lang: "fr"
identifier:
- scheme: 'ISBN-13'
  text: '978-3-0361-0095-1'
rights: "© 2020 Abrüpt, CC BY-NC-SA"
version: '1.0'
year: 2020
depot: troisième trimestre 2020
licence: 'Cet ouvrage est mis à disposition selon les termes de la Licence Creative Commons Attribution --- Pas d''Utilisation Commerciale --- Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0).'
lien_licence: 'https://abrupt.ch/partage'
informations_licence: 'Nous avons néanmoins une lecture adaptative de cette licence.'
lien_livre: 'https://abrupt.ch/jerome-orsoni/habitacles'
---
